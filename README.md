****Description du Projet********


Ce projet vise à automatiser la mise en place et la gestion d'une infrastructure complète pour le déploiement d'une application WordPress. L'infrastructure est composée de plusieurs types de serveurs, chacun ayant un rôle spécifique :


Serveur Web : Gère la réception du trafic Internet.

Serveur Applicatif : Héberge l'application WordPress.

Serveur de Base de Données : Gère la base de données pour WordPress.

Serveur Backup : Responsable de la récupération et du stockage des sauvegardes de la base de données.

**Domaine**
Nom de domaine : monblog.projet.local

Serveurs et Rôles

Serveur Web :
Nginx ou Apache pour la réception du trafic.
Configuration de la redirection HTTP vers HTTPS.

Serveur Applicatif :
Installation et configuration de WordPress.
Configuration pour la répartition de charge.

Serveur de Base de Données :
PostgreSQL ou MySQL comme système de gestion de base de données.
Configuration pour la haute disponibilité et la répartition de charge.

Serveur Backup :
Mise en place de stratégies de sauvegarde régulières et sécurisées.


**Dépendances**

Ansible
Serveur Linux (Ubuntu, CentOS, etc.)
Nginx/Apache
MySQL/PostgreSQL
PHP, PHP-FPM (pour WordPress)



_Exécutez le playbook Ansible pour déployer l'infrastructure _:

ansible-playbook -i inventory_hostname site.yml

**Ajout de Serveurs ou Services**
Pour ajouter de nouveaux serveurs ou services, modifiez le fichier d'inventaire et ajustez les playbooks et rôles en conséquence.
